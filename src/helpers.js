import axios from 'axios'
import React from "react";
const API = 'https://jsonplaceholder.typicode.com/';

export function getAPI(path) {
    return (API + path);

}

async function getData(path) {
    return await axios.get(path);
}

export async function userPhoto(id) {
    const result = await getData(getAPI('albums?userId=' + id));
    let num = Math.floor((Math.random() * (result.data.length - 1)));
    return await albumPhoto(result.data[num].id);
}

export async function albumPhoto(id) {
    const result = await getData(getAPI('photos?albumId=' + id));
    let num = Math.floor((Math.random() * (result.data.length - 1)));
    return result.data[num];
}

export async function singlePhoto(id) {
    const result = await getData(getAPI('photos?id=' + id));
    return result.data[0];
}


export function loading(isLoading) {
    if ( isLoading ) {
        return (
            <div className="d-flex justify-content-center">
                <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        )
    }
}