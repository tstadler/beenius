import React, { Component } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
import {getAPI, loading} from "./helpers";
import LoadPhoto from "./LoadPhoto";

class User extends Component {
    constructor(props) {
        super(props);

        this.state = {
            albums: [],
            user: props.location.state.user,
            isLoading: false,
        };
    }

    async componentDidMount() {
        this.setState({ isLoading: true })

        try {
            const user = this.state.user;
            const result = await axios.get(getAPI( 'albums?userId=' + user.id));

            this.setState({
                albums: result.data,
                isLoading: false
            })
        } catch {
            this.setState({
                isLoading: false
            })
        }
    }

    render() {
        const { albums, isLoading, user } = this.state;

        loading(isLoading);

        return (
            <div className='content-wrapper'>
                <div className='content-header'>
                    <div className='container position-relative'>
                        <h1>Albums for { user.name }</h1>
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                            <li className="breadcrumb-item active">{ user.name }</li>
                        </ol>
                    </div>
                </div>
                <div className='content'>
                    <div className='container'>
                        <div className='row'>
                            { albums.map(function(hit, i) {
                                return(
                                    <div key={i} className="col-md-3">
                                        <Link to={{ pathname: '/album', state: { user: user, album: hit } }} className="card mb-3">
                                            <LoadPhoto id={hit.id} type={'album'} />
                                            <div className="card-body">
                                                <p className='card-text'>{ hit.title }</p>
                                            </div>
                                        </Link>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default User