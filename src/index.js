import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/_app.css';
import Header from "./partial/Header";
import Footer from "./partial/Footer";
import Home from "./Home";
import User from "./User";
import Album from "./Album";
import Photo from "./Photo";

class App extends Component {

    render() {
        return (
            <BrowserRouter>
                <div className='d-flex flex-column h-100'>
                    <Header/>
                    <Switch>
                        <Route exact path='/' component={Home}/>
                        <Route exact path='/user' component={User}/>
                        <Route exact path='/album' component={Album}/>
                        <Route exact path='/photo' component={Photo}/>
                    </Switch>
                    <Footer/>
                </div>
            </BrowserRouter>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('app'))