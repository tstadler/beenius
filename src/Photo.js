import React, { Component } from 'react'
import axios from 'axios'
import {getAPI} from "./helpers";

class Photo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            photoId: props.location.state.photo,
            album: props.location.state.album,
            user: props.location.state.user,
            isLoading: false,
            isToggleOn: false
        };

        this.showDetail = this.showDetail.bind(this);
        this.goBack = this.goBack.bind(this);
    }

    async componentDidMount() {
        this.setState({ isLoading: true })
        try {
            const photo = await axios.get(getAPI('photos?id=' + this.state.photoId));

            this.setState({
                photo: photo.data[0],
                isLoading: false
            })
        } catch {
            this.setState({
                isLoading: false
            })
        }
    }

    showDetail(){
        this.setState(state => ({
            isToggleOn: !state.isToggleOn
        }));
    }

    goBack(){
        this.props.history.goBack();
    }

    render() {
        const { photo, user, album } = this.state;
        let detailClass = ["details-overlay"];
        if(this.state.isToggleOn) {
            detailClass.push('hide')
        }

        try {
            return (
                <div className="fullscreen">
                    <img src={ photo.url } alt={ photo.title } onClick={this.showDetail} />
                    <div className={detailClass.join(' ')}>
                        <h4>Photo: { photo.title }</h4>
                        <h5>Album: { album.title }</h5>
                        <h6>Author { user.name }</h6>
                        <button type="button" onClick={this.goBack} className="close" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            )
        } catch {
            return(
                <div className="fullscreen">
                    <div className="text-center m-5">
                        <div className="spinner-border" role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

export default Photo