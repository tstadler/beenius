import React from 'react'
import { Link } from 'react-router-dom'

const Header = () => (
    <header>
        <div className="navbar navbar-dark bg-dark">
            <div className="container">
                <a href="/" className="navbar-brand">Web Coding Challenge</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader"
                        aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
        <div className="collapse" id="navbarHeader">
            <div className="container">
                <div className="row">
                    <div className="col-sm-12">
                        <ul className="nav pt-2 pb-2">
                            <Link className="nav-link" to="/">Home</Link>
                            <Link className="nav-link" to="#">About</Link>
                            <Link className="nav-link" to="#">Subscription</Link>
                            <Link className="nav-link" to="#">Contact</Link>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
)

export default Header