import React from 'react'

const Footer = () => (
    <footer className="container">
        <p>&copy; Tadej Štadler</p>
    </footer>
)

export default Footer