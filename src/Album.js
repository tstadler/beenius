import React, { Component } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
import {getAPI, loading} from "./helpers";
import LoadPhoto from "./LoadPhoto";

class Album extends Component {
    constructor(props) {
        super(props);

        this.state = {
            photos: [],
            album: props.location.state.album,
            user: props.location.state.user,
            isLoading: false,
        };
    }

    async componentDidMount() {
        this.setState({ isLoading: true })

        try {
            const album = this.state.album;
            const result = await axios.get(getAPI('photos?albumId=' + album.id));

            this.setState({
                photos: result.data,
                isLoading: false
            })
        } catch {
            this.setState({
                isLoading: false
            })
        }
    }

    render() {
        const { photos, isLoading, album, user } = this.state;

        loading(isLoading);

        return (
            <div className='content-wrapper'>
                <div className='content-header'>
                    <div className='container position-relative'>
                        <h1>Album: { album.title } </h1>
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                            <li className="breadcrumb-item"><Link to={{ pathname: '/user', state: { user: user } }}>{ user.username }</Link></li>
                            <li className="breadcrumb-item active">{ album.title }</li>
                        </ol>
                    </div>
                </div>
                <div className='content'>
                    <div className='container'>
                        <div className='row'>
                            { photos.map(function(hit, i) {
                            return (
                                <div key={i} className="image-container col-md-2 mb-4">
                                    <Link to={{ pathname: '/photo', state: { user: user, album: album, photo: hit.id } }}>
                                        <LoadPhoto id={hit.id} type={'photo'} />
                                        <div className="text-overlay">
                                            <span>{ hit.title }</span>
                                        </div>
                                    </Link>
                                </div>
                            )
                            })}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Album