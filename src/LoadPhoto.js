import React, { Component } from 'react'
import {albumPhoto, singlePhoto, userPhoto} from "./helpers";

class LoadPhoto extends Component {
    constructor(props) {
        super(props);

        this.state = {
          photo: null
        };
    }

    async componentDidMount() {
        this.setState({ isLoading: true })
        const id = this.props.id;
        const type = this.props.type;

        try {
            let photo;

            switch(type){
                case 'user':
                    photo = await userPhoto(id);
                    break;
                case 'album':
                    photo = await albumPhoto(id);
                    break;
                default:
                    photo = await singlePhoto(id);
                    break;
            }

            this.setState({
                photo: photo
            })
        } catch {

        }
    }

    render() {
        const photo = this.state.photo;

        try {
            return (
                <img src={ photo.thumbnailUrl } className="card-img-top" alt={ photo.title } />
            )
        } catch {
            return(
                <div className="text-center m-5">
                    <div className="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
            )
        }
    }
}

export default LoadPhoto

