import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import {getAPI, loading} from './helpers'
import LoadPhoto from "./LoadPhoto";

class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            users: [],
            isLoading: false,
        };
    }

    async componentDidMount() {
        this.setState({ isLoading: true })
        let API = getAPI('users');

        try {
            const result = await axios.get(API);
            this.setState({
                users: result.data,
                isLoading: false
            })
        } catch {
            this.setState({
                isLoading: false
            })
        }
    }

    render() {
        const { users, isLoading } = this.state;

        loading(isLoading);

        return (
            <div className='content-wrapper'>
                <div className='content-header'>
                    <div className='container position-relative'>
                        <h1>Grid view of all users</h1>
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item active">Home</li>
                        </ol>
                    </div>
                </div>
                <div className='content'>
                    <div className='container'>
                        <div className='users row'>
                            { users.map(function(hit, i) {
                                return (
                                    <div key={i} className="col-md-3">
                                        <Link to={{ pathname: '/user', state: { user: hit } }} className="card mb-4">
                                            <LoadPhoto id={hit.id} type={'user'} />
                                            <div className="card-body">
                                                <h5 className='card-title'>{ hit.username }</h5>
                                                <h6 className='card-subtitle mb-2 text-muted'>{ hit.name }</h6>
                                                <p className='card-text'>{ hit.email }</p>
                                            </div>
                                        </Link>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Home